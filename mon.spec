#
# spec file for package mon (Version 1.3.4-3)
#
# Copyright (c) 2004 SUSE LINUX AG, Nuernberg, Germany.
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.
#
# Please submit bugfixes or comments via http://www.suse.de/feedback/
#

BuildRequires: bash bzip2 cpio cpp diffutils file filesystem findutils grep groff gzip info m4 make man patch sed tar texinfo autoconf automake binutils gcc libtool perl rpm gcc-c++ systemd

Name:         etbemon
Version:      1.3.4
Release:      3
Summary:      The etbemon network monitoring system
License:      GPL
Group:        System/Monitoring
URL:          https://doc.coker.com.au/projects/etbe-mon/
Source:       http://www.coker.com.au/etbemon/etbemon_%{version}.tgz
Requires:     perl
Requires:     perl(Time::Period)
Requires:     perl-Convert-BER
Requires:     fping
Requires:     perl-libwww-perl
Requires:     systemd
Requires:     perl-BSD-Resource
BuildRoot:    %{_tmppath}/%{name}-%{version}-build

%define	filelist %{name}-%{version}-filelist

%description
"etbemon" is a tool for monitoring the availability of services. Services
may be network-related, environmental conditions, or nearly anything
that can be tested with software.  It is extremely useful for system
administrators, but not limited to use by them. It was designed to be a
general-purpose problem alerting system, separating the tasks of
testing services for availability and sending alerts when things fail.
To achieve this, "etbemon" is implemented as a scheduler which runs the
programs which do the testing, and triggering alert programs when these
scripts detect failure.  None of the actual service testing or
reporting is actually handled by "etbemon". These functions are handled by
auxillary programs.

%{?systemd_ordering}


Authors:
--------
    Russell Coker <russell@coker.com.au>
    Jim Trocki <trockij@arctic.org>

%prep
###################################################################
%setup -q
%setup -T -D
###################################################################

%build
make -C mon.d CFLAGS="-g -O2" LDFLAGS="-Wl,-z,relro" CPPFLAGS=""
make -C bin
make -C mon-local.d
###################################################################

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_libdir}/mon/bin
mkdir -p %{buildroot}/%{_libdir}/mon/alert.d
mkdir -p %{buildroot}/%{_sbindir}
mkdir -p %{buildroot}/%{_mandir}/man1
mkdir -p %{buildroot}/%{_libdir}/mon/mon.d
mkdir -p %{buildroot}/%{_libdir}/mon/mon-local.d
mkdir -p %{buildroot}/%{_localstatedir}/lib/mon
mkdir -p %{buildroot}/%{_libdir}/mon/utils
mkdir -p %{buildroot}/%{_sysconfdir}/mon
mkdir -p %{buildroot}/%{_sysconfdir}/init.d
mkdir -p %{buildroot}/%{_sysconfdir}/logrotate.d
mkdir -p %{buildroot}%{_unitdir}
cp mon %{buildroot}/%{_sbindir}/
cp bin/*.helper %{buildroot}/%{_libdir}/mon/bin
cp alert.d/*.alert %{buildroot}/%{_libdir}/mon/alert.d
chmod 755 %{buildroot}/%{_libdir}/mon/alert.d/*
rm %{buildroot}/%{_libdir}/mon/alert.d/snpp.alert
cp ./clients/moncmd %{buildroot}/%{_sbindir}/moncmd
cp ./clients/monshow %{buildroot}/%{_sbindir}/monshow
cp mon.d/*.monitor %{buildroot}/%{_libdir}/mon/mon.d
cp mon-local.d/*.monitor mon-local.d/*.monitor.real -p %{buildroot}/%{_libdir}/mon/mon-local.d
cp -a ./doc/*.1 %{buildroot}/%{_mandir}/man1/
cp ./etc/mon.cf %{buildroot}/%{_sysconfdir}/mon/mon.cf
mv ./etc/auth.cf %{buildroot}/%{_sysconfdir}/mon
cp -a ./utils/ %{buildroot}/%{_libdir}/mon/
mkdir -p %{buildroot}/sbin
cp systemd/mon.service %{buildroot}%{_unitdir}/etbemon.service
# clean up after perl module install - remove special files
find %{buildroot} -name "perllocal.pod" -o -name ".packlist" -o -name "*.bs" |xargs -i rm -f {}
# build filelist 
echo "%defattr(-,root,root)" > %filelist
find %{buildroot} -type f -printf "/%%P\n" | sed -e s/1$/1.gz/ >> %filelist

[ -z %filelist ] && {
    echo "ERROR: EMPTY FILE LIST"
    exit -1
}

###################################################################

%files -f %filelist
%doc doc/*

%doc CHANGES COPYING COPYRIGHT CREDITS INSTALL KNOWN-PROBLEMS README
%doc TODO VERSION
###################################################################

%clean
if
  [ -z "${RPM_BUILD_ROOT}"  -a "${RPM_BUILD_ROOT}" != "/" ]
then
  rm -rf $RPM_BUILD_ROOT
fi
rm -rf $RPM_BUILD_ROOT
###################################################################

%preun
/usr/bin/systemctl --no-reload disable %{pkgname}.service >/dev/null 2>&1 || :
/usr/bin/systemctl stop %{pkgname}.service >/dev/null 2>&1 ||:
if [ -r %{_localstatedir}/run/mon.pid ]; then
	/etc/init.d/mon stop
fi
###################################################################

%post
/usr/bin/systemctl preset %{pkgname}.service >/dev/null 2>&1 ||:
if [ -d %{_localstatedir}/log -a ! -f %{_localstatedir}/log/mon_history.log ]; then
    touch %{_localstatedir}/log/mon_history.log
fi

if [ $1 = 1 ]; then
    /sbin/chkconfig --add mon
fi

###################################################################

%postun
/usr/bin/systemctl daemon-reload >/dev/null 2>&1 ||:
if [ "$1" = "0" -a -f %{_localstatedir}/log/mon_history.log ]; then
    rm -f %{_localstatedir}/log/mon_history.log
fi

%changelog -n mon
* Sun Sep 20 2020 - russell@coker.com.au
- Update to 1.3.4
* Wed Jul 07 2004 - eric@transmeta.com
- update to 1.0.0pre2, remove suse-ness
* Mon Mar 01 2004 - hmacht@suse.de
- building as nonroot-user
* Fri Feb 27 2004 - kukuk@suse.de
- Cleanup neededforbuild
- fix compiler warnings
* Mon Feb 10 2003 - lmb@suse.de
- Fixed path to comply with FHS.
* Fri Oct 18 2002 - lmb@suse.de
- Fix for Bugzilla #21086: init script had a broken path and syntax
  error.
* Tue Aug 20 2002 - lmb@suse.de
- Fix for Bugzilla # 17936; PreRequires corrected.
* Mon Aug 12 2002 - lmb@suse.de
- Perl dependencies updated for Perl 5.8.0
* Fri Jul 26 2002 - lmb@suse.de
- Perl dependencies adjusted to comply with SuSE naming scheme
* Fri Jul 26 2002 - lmb@suse.de
- Adapted from Conectiva to UnitedLinux
- init script cleanup
* Wed Jul 24 2002 - F�bio Oliv� Leite <olive@conectiva.com.br>
- Version: mon-0.99.2-1ul
- Adapted for United Linux
* Sat Jul 20 2002 - Claudio Matsuoka <claudio@conectiva.com>
- Version: mon-0.99.2-3cl
- updated dependencies on perl modules to lowercase names
* Thu May 16 2002 - F�bio Oliv� Leite <olive@conectiva.com.br>
- Version: mon-0.99.2-2cl
- Added %%attr to %%{_libdir}/mon/*, so that the helper scripts are executable
  Closes: #5522 (aparente problema com as permiss�es)
- Changed initscript to use gprintf
  Closes: #4172 (Internacionaliza��o (?))
* Fri Dec 28 2001 - Ricardo Erbano <erbano@conectiva.com>
- Version: mon-0.99.2-1cl
- New upstream relase 0.99.2
* Sat Nov 17 2001 - Claudio Matsuoka <claudio@conectiva.com>
- Version: mon-0.38.20-6cl
- fixed doc permissions
* Thu Jun 21 2001 - Eliphas Levy Theodoro <eliphas@conectiva.com>
- Version: mon-0.38.20-5cl
- fixed initscript - /usr/lib/mon -> /usr/sbin (Closes: #3792)
- added requires for perl-Convert-BER
- added post{,un} scripts to handle logfile mon_history.log
* Fri Mar 23 2001 - Luis Claudio R. Gon�alves <lclaudio@conectiva.com.br>
- Version: mon-0.38.20-4cl
- fixed the initscript (it was missing a "-f" switch)
* Tue Oct 31 2000 - Arnaldo Carvalho de Melo <acme@conectiva.com.br>
- %%{_sysconfdir}/mon is part of this package
- small cleanups
* Thu Sep 28 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- Wrong version in the mon-perl dependency...
* Thu Sep 21 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- Updated to 0.38.20.
* Fri Jun 16 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- Fixed TIM alert, added history file, added logrotate script
* Mon Jun 12 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- Added an alert via TIM Celular cellphones
* Thu Jun 08 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- Made the %%preun nicer
* Thu Jun 01 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- New spec format
* Mon Apr 17 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- Added a new monitor (initscript.monitor)
* Fri Apr 14 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- Added proxy support to http.monitor
* Thu Apr 13 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- Fixed a small bug in the init script
- Added scripts to alert via Mobi pagers and Global Telecom cellphones
* Mon Apr 10 2000 - F�bio Oliv� Leite <olive@conectiva.com>
- Initial RPM packaging
