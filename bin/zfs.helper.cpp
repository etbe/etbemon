#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
  if(argc != 2)
  {
    fprintf(stderr, "ERROR: Must specify pool name of ZFS filesystem\n");
    return 1;
  }
  char * const child_args[] = { "/sbin/zpool", "status", argv[1], NULL };
  execv(child_args[0], child_args);
  fprintf(stderr, "ERROR: Can't execute %s\n", child_args[0]);
  return 1;
}

