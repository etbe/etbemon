#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
  if(setgid(0) || setuid(0))
  {
    fprintf(stderr, "ERROR: setgid(0)/setuid(0) failed\n");
    return 1;
  }
  char * const child_args[] = { "/sbin/hplog", "-t", NULL };
  execv(child_args[0], child_args);
  fprintf(stderr, "ERROR: Can't execute %s\n", child_args[0]);
  return 1;
}

