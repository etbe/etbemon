#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
  if(argc != 2)
  {
    fprintf(stderr, "ERROR: Must specify mountpoint of BTRFS filesystem\n");
    return 1;
  }
  pid_t pid = fork();
  if(-1 == pid)
  {
    fprintf(stderr, "ERROR: Fork failed\n");
    return 1;
  }
  else if(pid)
  {
    int wstatus = 0;
    if(wait(&wstatus) == -1)
    {
      fprintf(stderr, "ERROR: wait(2) error\n");
      return 1;
    }
    if(!WIFEXITED(wstatus) || WEXITSTATUS(wstatus))
    {
      fprintf(stderr, "ERROR: Error running btrfs device stats\n");
      return 1;
    }
  }
  else
  {
    char * const child_args[] = { "/bin/btrfs", "device", "stats", argv[1], NULL };
    execv(child_args[0], child_args);
    fprintf(stderr, "ERROR: Can't execute %s\n", child_args[0]);
    return 1;
  }

  pid = fork();
  if(-1 == pid)
  {
    fprintf(stderr, "ERROR: Fork failed\n");
    return 1;
  }
  else if(pid)
  {
    int wstatus = 0;
    if(wait(&wstatus) == -1)
    {
      fprintf(stderr, "ERROR: wait(2) error\n");
      return 1;
    }
    if(!WIFEXITED(wstatus) || WEXITSTATUS(wstatus))
    {
      fprintf(stderr, "ERROR: Error running btrfs subvol list\n");
      return 1;
    }
  }
  else
  {
    char * const child_args[] = { "/bin/btrfs", "subvol", "list", argv[1], NULL };
    execv(child_args[0], child_args);
    fprintf(stderr, "ERROR: Can't execute %s\n", child_args[0]);
    return 1;
  }
}

