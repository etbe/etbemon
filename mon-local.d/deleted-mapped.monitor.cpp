#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
  char * const child_args[] = { "/usr/lib/mon/mon-local.d/deleted-mapped.monitor.real", NULL };
  execv(child_args[0], child_args);
  fprintf(stderr, "ERROR: Can't execute %s\n", child_args[0]);
  return 1;
}

