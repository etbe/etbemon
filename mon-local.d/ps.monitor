#!/usr/bin/perl
use strict;

# Monitors processes on the local host.  Requires Proc::ProcessTable

# normal usage:
#    monitor ps.monitor ( process:[min]-[max] )+ ;;
#
# where:
#    process	process name
#    min	optional minimum number of process name running
#    max	optional maxumum number of process name running
#
# example:
#    monitor ps.monitor dhcpd3:1-1 syslog-ng:1- ;;
#
# debugging usage:
# /usr/lib/mon/mon-local.d/ps.monitor -x PID
#
# where PID is the PID that you want to know the name for where
# Proc::ProcessTable doesn't agree with ps on the process name


#Copyright 2005 Allan Wind
#
# Updated by Russell Coker 2020
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
#of the Software, and to permit persons to whom the Software is furnished to do
#so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

use Proc::ProcessTable;

# parse command line
my %watched_processes;
my $special_pid = 0;
if($ARGV[0] =~ /-x/)
{
  $special_pid = $ARGV[1];
}
else
{
  while(<@ARGV>)
  {
	if(m/([^:]+):([0-9]*)?-([0-9]*)?/) {
		# $1 is process, $2 is min and $3 is max
		$watched_processes{$1} = [$2, $3, 0];
	} else {
		print "Can't parse \"$_\"\n";
		exit 1;
	}
  }
}

my @interpreters = ( "perl", "python", "python3", "python3.9", "lua5.1", "lua5.2", "lua5.4" );

# read process table and count the processes of interest
my $process_table = new Proc::ProcessTable('cache_ttys' => 0 );

foreach my $process ( @{$process_table->table} ) {
	if($special_pid && $process->pid != $special_pid)
	{
		next;
	}
	my $name = $process->fname;
	$name = substr($name, rindex($name, "/") + 1);
	$name =~ s/ .*$//;

	foreach my $i (@interpreters)
	{
		if($name eq $i)
		{
			my @cmd = split(/ /, $process->cmndline);
			$name = substr($cmd[1], rindex($cmd[1], "/") + 1);
			last;
		}
	}

	if($special_pid)
	{
		print "PID:$special_pid has name \"$name\"\n";
		exit(0);
	}

	if( defined($watched_processes{$name}) ) {
		$watched_processes{$name}[2]++;
	}
}

my $summary;
my $detail;

# print name:count of processes if needed
my $report = 0;
for my $process (keys(%watched_processes))
{

	my $count = $watched_processes{$process}[2];
	my $min = $watched_processes{$process}[0] || $count;
	my $max = $watched_processes{$process}[1] || $count;

	if($min > $count || $max < $count)
	{
		$report = 1;
		$summary .= "$process:$watched_processes{$process}[2] ";
		if($min > $count)
		{
			$detail .= "$process:$watched_processes{$process}[2] < $min ($min-$max)\n";
		}
		else
		{
			$detail .= "$process:$watched_processes{$process}[2] > $max ($min-$max)\n";
		}
	}
}

if($report)
{
  print "$summary\n$detail";
}
exit $report;
