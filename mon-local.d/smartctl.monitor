#!/usr/bin/perl
use strict;
use Getopt::Std;

# copyright 2020 Russell Coker, Licensed under GPLv3 or higher.
#
# Monitor script to run smartctl and parse output.  By default checks
# /dev/sd[a-z] /dev/sd[a-z][a-z] /dev/nvme[0-9] /dev/nvme[0-9][0-9] but
# you can give a list of devices on the command line
#
# -f to treat all marginal conditions as failed
# -m ATTRIBUTE_NAME to treat attribute as marginal
#
# -M for MegaRAID support, takes parameters of a device node on the MegaRAID
# and a list of disk IDs separated by commas, eg "-M /dev/sda,0,1,7" for
# disks 0, 1, and 7 on the MegaRAID controller that runs /dev/sda.

my $summary;
my $margsummary;
my $detail;
my $megadev;
my @ids;

our $opt_f;
our $opt_m;
our $opt_M;
getopts("fm:M:") or die;

if($opt_M)
{
  @ids = split(/,/, $opt_M);
  $megadev = $ids[0];
  shift @ids;
}
else
{
  if(-1 == $#ARGV)
  {
    @ids = glob( "/dev/sd[a-z] /dev/sd[a-z][a-z] /dev/nvme[0-9] /dev/nvme[0-9][0-9]" );
    if(-1 == $#ids)
    {
      print "No devices found\n";
      exit(1);
    }
  }
  else
  {
    @ids = @ARGV;
  }
}

my %results;

foreach my $dev (@ids)
{
  my $days_used = 0;
  my $percent_used = 0;
  my $cmd;
  if($opt_M)
  {
    $cmd = "/usr/lib/mon/bin/smartctl.helper M$megadev,$dev"
  }
  else
  {
    $cmd = "/usr/lib/mon/bin/smartctl.helper $dev";
  }
  open(SMART, "$cmd|") or die "Can't run smartctl.helper";
  my $header = 0;
  my $sector_size = 512;
  while(<SMART>)
  {
    if($_ =~ /^Sector Size/)
    {
      chomp;
      $_ =~ s/bytes logical.physical.*$//;
      $_ =~ s/bytes physical.*$//;
      $sector_size = (split(/ /, $_))[-1];
    }
    elsif($_ =~ /^=== START OF .*SMART DATA SECTION/)
    {
      $header = 1;
      last;
    }
  }
  if(not $header)
  {
    die "Error running \"$cmd\", does device exist?";
  }
  my $line = <SMART>;
  my $name = $dev;
  $name =~ s/^.dev.//;
  my $attribute;
  my $failstate = 0;
  my $marginal = 0;
  if($line =~ /FAILED/)
  {
    $results{$dev} = "FAILED";
    $failstate = 1;
  }
  else
  {
    $line = <SMART>;
    if($line =~ /Please note the following marginal Attributes/)
    {
      $results{$dev} = "MARGINAL";
      $marginal = 1;
    }
    else
    {
      $results{$dev} = "OK";
    }
  }

  while(<SMART>)
  {
    if($dev =~ /^.dev.nvme/)
    {
      if($_ =~ /^Data Units Written/)
      {
        chomp;
        $_ =~ s/^Data Units Written.*\[//;
        $_ =~ s/\]/W/;
        $results{$dev} .= " ($_)";
      }
    }
    if($_ =~ /^ID/)
    {
      while(<SMART>)
      {
        if(length($_) < 3)
        {
          last;
        }
        if($_ =~ /Total_LBAs_Written/)
        {
          $_ =~ s/^.*- *//;
          # GBW
          $_ = $_ / 1000000 * $sector_size / 1000;
          if($_ > 9999)
          {
            $_ = $_ / 1000;
            $results{$dev} .= sprintf(" (%d TBW)", $_);
          }
          else
          {
            $results{$dev} .= sprintf(" (%d GBW)", $_);
          }
        }
        elsif($_ =~ /Power.On.Hours/)
        {
          chomp;
          $_ = (split(/ /, $_))[-1];
          $_ = join("", split(/,/, $_));
          $days_used = $_/24;
        }
      }
    }
    if($_ =~ /Power On Hours/)
    {
      chomp;
      $_ = (split(/ /, $_))[-1];
      $_ = join("", split(/,/, $_));
      $days_used = $_/24;
    }
    elsif($_ =~ /Percentage Used/)
    {
      chomp;
      $_ = (split(/ /, $_))[-1];
      $percent_used = join("", split(/,/, $_));
    }
  }
  if($percent_used > 0)
  {
    if($days_used > 0)
    {
      if($percent_used >= 100)
      {
        $results{$dev} .= sprintf(" (%d days used, %d%% of writes)", $days_used, $percent_used);
      }
      else
      {
        $results{$dev} .= sprintf(" (%d days used, %d%% of writes, %d days to go)", $days_used, $percent_used, $days_used / $percent_used * (100 - $percent_used));
      }
    }
    else
    {
      $results{$dev} .= sprintf(" (%d%% used)", $percent_used);
    }
  }
  elsif($days_used > 0)
  {
    $results{$dev} .= sprintf(" (%d days)", $days_used);
  }
  $attribute =~ s/ $//;
  if($failstate and not $attribute eq $opt_m)
  {
    $results{$dev} .= " FAIL($attribute)";
  }
  else
  {
    if($marginal)
    {
      $margsummary .= "$name MARGINAL($attribute) ";
    }
  }
  close(SMART);
}
if($summary or ($margsummary and $opt_f))
{
  print "$summary$margsummary\n";
  foreach my $key (sort(keys %results))
  {
    print "$key $results{$key}\n";
  }
  exit(1);
}

if($margsummary)
{
  print "$margsummary\n";
  foreach my $key (keys %results)
  {
    print "$key $results{$key}\n";
  }
}
else
{
  if($opt_M)
  {
    print "MegaRAID $opt_M OK\n";
  }
  else
  {
    print "SMART OK\n";
  }
  print $detail;
}
foreach my $key (keys %results)
{
  print "$key $results{$key}\n";
}
exit(0);
