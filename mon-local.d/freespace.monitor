#!/usr/bin/perl
use strict;
use Filesys::Df;
use Sys::Filesystem;
#
# Monitor disk space usage
#
# Arguments are:
#
# path:freespace[:freeinodes] [path:freespace[:freeinodes]...]
# or
# path:free%[:freeinodes] [path:free%[:freeinodes]...]
#
# This script will exit with value 1 if "path" has less than
# "freespace" or less than "free" percent available.
# the freespace defaults to kilobytes but can have M, G, or T appended to
# measure in MiB, GiB, or TiB.
#
# This script will check for the path being a mountpoint, if it is not then
# assume that filesystem was umounted and give error
#
# The first output line is a list of the paths which failed, and
# how much space is free, in megabytes.
#
# If you are testing NFS-mounted directories, should probably
# mount them with the ro,intr,soft options, so that operations
# on those mount points don't block forever if the server is
# down, and I may eventually change this code to use an alarm(2)
# to interrupt the stat and statfs system calls.
#
# This requires Fabien Tassin's Filesys::Df module, available from 
# your friendly neighborhood CPAN mirror. See http://www.perl.com/perl/
# That is in the Debian package libfilesys-df-perl
# Also requires Debian package libsys-filesystem-perl
# 
# Jim Trocki, trockij@arctic.org
#
# Russell Coker <russell@coker.com.au>
# Added support for MiB, GiB, and TiB
# Added check for Inodes (default 10%)
# Made it work with Strict.
#
# $Id: freespace.monitor,v 1.2 2005/04/17 07:42:27 trockij Exp $
#
#  Copyright (C) 1998, Jim Trocki
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

my @failures;

my $fs = new Sys::Filesystem;
my @mounts = $fs->mounted_filesystems();

foreach (@ARGV)
{
  my ($path, $minavail, $mininodes) = split (/:/, $_, 3);

  if(not grep(/^$path$/, @mounts))
  {
    push (@failures, "$path is not a mount point");
    next;
  }

  my $ref = df ($path);

  if (!defined($ref))
  {
    push (@failures, "statfs error for $path: $!");
    next;
  }

# for debugging
#foreach my $key (keys %$ref)
#{
#  print "$key:$ref->{$key}\n";
#}

  if ($minavail =~ /(\d+(\.\d+)?)%/o) {
    $minavail = int(($ref->{blocks}) * $1 / 100);
  }
  elsif ($minavail =~ /[Mm]$/) {
    $minavail *= 1024;
  }
  elsif ($minavail =~ /[Gg]$/) {
    $minavail *= 1024 * 1024;
  }
  elsif ($minavail =~ /[Tt]$/) {
    $minavail *= 1024 * 1024 * 1024;
  }

  if (defined($mininodes))
  {
    if(!defined($ref->{files}))
    {
      push (@failures, sprintf ("%s doesn't tell us how many Inodes are free", $path));
      next;
    }
    if($mininodes =~ /(\d+(\.\d+)?)%/o)
    {
      $mininodes = int(($ref->{files}) * $1 / 100);
    }
  }
# by default check for 10% free Inodes
  elsif (defined($ref->{files}))
  {
    $mininodes = int(($ref->{files}) / 10);
  }

  if ($ref->{bavail} < $minavail)
  {
    my $meg = $ref->{bavail} / 1024;
    if($meg < 1024)
    {
      push (@failures, sprintf ("%1.1fMiB free on %s", $meg, $path));
    }
    else
    {
      push (@failures, sprintf ("%1.1fGiB free on %s", $meg / 1024, $path));
    }
  }

  if (defined($mininodes) && $ref->{ffree} < $mininodes)
  {
    push (@failures, sprintf ("%d inodes free on %s", $ref->{ffree}, $path));
  }
}

if (@failures)
{
  print join (", ", @failures), "\n";
  exit 1;
}

exit 0;
