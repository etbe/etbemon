#!/usr/bin/perl
use strict;
use Getopt::Std;

# Check for programs that have deleted files mapped, this can be due to
# packages replaced to fix security issues.
# Don't check systemd programs (which can cause problems if restarted in
#
# The option -u specifies the maximum UID to report on, default 999
# (Debian regular users start at 1000)

our $opt_u = 999;
getopts("u:");

opendir(my $dh, "/proc") || die "Can't opendir /proc: $!";
my @procs = grep { /^[0-9]+$/ && -d "/proc/$_" } readdir($dh);
closedir $dh;

my %uniq_file;
my %root_procs;
my %non_root_procs;
foreach ( @procs )
{
  my $pid_num = $_;

  my $proc_name = readlink("/proc/$pid_num/exe");
  if($proc_name =~ /^\/lib\/systemd\/systemd/ or $proc_name eq "/usr/bin/dbus-daemon")
  {
    next;
  }

  if($proc_name eq "/usr/bin/sudo")
  {
    next;
  }

  if($proc_name =~ /^\/usr\/sbin\/sshd/ && open(CMD, "</proc/$pid_num/cmdline"))
  {
    my $name = <CMD>;
    close(CMD);
    if($name =~ /sshd: [a-z][a-z0-9]* .priv/)
    {
      next;
    }
  }

  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks)
       = stat("/proc/$pid_num");
  if($uid > $opt_u)
  {
    next;
  }

  my $maps_file = "/proc/$_/maps";
  my $status_file = "/proc/$_/status";
  if(not open(MAP, "<$maps_file"))
  {
    next;
  }
# files to ignore
# /memfd: is for memfd https://dvdhrm.wordpress.com/tag/memfd/
# what is /[aio] ?
# ignore things under /home as some programs map files in user home dir and
# unlink them.
# ignore /run/user for files under $XDG_RUNTIME_DIR (/run/user/$UID)
# ignore /dev/zero as that is different for each map and thus looks deleted
# ignore /tmp/ as Python creates temporary files there for mapping
# ignore /var/lib as lots of programs manage their own temporary files there
# ignore /i915 which is used by some X apps on systems with Intel video
#
# temporarily ignore /etc/selinux/default/contexts/files/* for systemd
# temporarily ignore /var/log/journal for systemd bug
  my @del = grep { !/\/var\/log\/journal/ } grep { !/\/etc\/selinux\/default/ } grep { !/\/i915/ } grep { !/\/var\/lib\// } grep { !/\/tmp\// } grep { !/\/home/ } grep { !/\/run\/user/ } grep { !/\/dev\/zero/ } grep { !/\/dev\/shm\// } grep { !/\/SYSV/ } grep { !/\/memfd:/ } grep { !/\/\[aio\]/ } grep { /deleted.$/ } <MAP>;
  close(MAP);
  next if($#del == -1);

  for (@del) 
  {
    chomp;
    s/^.*  //;
    s/..deleted.*$//;
    $uniq_file{$_}++;
  }
  my $uid = 1;
  if(not open(STATUS, "<$status_file"))
  {
    $uid = 0;
  }
  else
  {
    while(<STATUS>)
    {
      if($_ =~ /^Uid:/ or $_ =~ /^Gid:/)
      {
        chomp;
        my @uid_arr = split(' ', $_);
        if($uid_arr[1] == 0 or $uid_arr[2] == 0 or $uid_arr[3] == 0 or $uid_arr[4] == 0)
        {
          $uid = 0;
        }
      }
    }
  }
  if($proc_name)
  {
    if($uid == 0)
    {
      $root_procs{$proc_name} .= " $pid_num";
    }
    else
    {
      $non_root_procs{$proc_name} .= " $pid_num";
    }
  }

#  printf("PID:%s, deleted: %s\n", $_, join(' ', keys(%uniq_file)));
}

my $file_count = keys %uniq_file;
if($file_count == 0)
{
  exit(0);
}
my $root_count = keys %root_procs;
my $non_root_count = keys %non_root_procs;
if($root_count == 0)
{
  print "$file_count deleted mapped files, $non_root_count processes\n";
}
else
{
  if($non_root_count == 0)
  {
    print "$file_count deleted mapped files, $root_count root owned processes\n";
  }
  else
  {
    print "$file_count deleted mapped files, $root_count root owned and $non_root_count non-root processes\n";
  }
  print "$root_count root programs:\n";
  for(keys %root_procs){
    print "$_: $root_procs{$_}\n";
  }
  print "\n";
}

if($non_root_count != 0)
{
  print "$non_root_count non-root programs:\n";
  for(keys %non_root_procs){
    print "$_: $non_root_procs{$_}\n";
  }
  print "\n";
}

print "$file_count deleted mapped files:\n";

for(keys %uniq_file){
 print("$_\n");
}
exit(1);
