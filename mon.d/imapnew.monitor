#!/usr/bin/perl
use strict;

# needs package libmail-imapclient-perl on Debian

# Mon script to talk to IMAP server, look for new messages, and DELETE old
# messages.  Use this with something like smtpswaks.monitor to send mail.
# Copyright Russell Coker GPLv3

# Options: -h target_host -u username -p password -P password_file
# -f folder (default "INBOX") -s for ssl (default no ssl) -t checktime
# -k max clock skew (seconds that a message appears to have gone back in time)
# default 1
# -d max delay in seconds (default half checktime)

# the -P option specifies a file containing the password so ps won't reveal
# it.

# The -t option specifies how recently mail must have been received, if you
# specify -t 300 (the default) then if there was no new mail in the last 5
# minutes then it's a failure

use Getopt::Std;

our $opt_d;
our $opt_f;
our $opt_h;
our $opt_k;
our $opt_p;
our $opt_P;
our $opt_u;
our $opt_s;
our $opt_t;
getopts("d:f:h:k:p:P:st:u:");

if(!$opt_h || !$opt_u || (!$opt_p && !$opt_P))
{
  printf("Must specify host, user, and password.\n");
  exit(1);
}

my $timeout = $opt_t ? $opt_t : 300;
my $max_delay = $opt_d ? $opt_d : $timeout / 2;
my $max_skew = $opt_k ? $opt_k : 1;

if($opt_P)
{
  open(PASSWORD, "<$opt_P") or die "Can't open password file $opt_P";
  $opt_p = <PASSWORD>;
  close(PASSWORD);
  chomp $opt_p;
}

use Mail::IMAPClient;
use Date::Parse;

my $imap = Mail::IMAPClient->new(
  Server      => $opt_h,
  User        => $opt_u,
  Password    => $opt_p,
  Ssl         => $opt_s ? 1 : 0,
  Uid         => 1,
  );

my $IMAP_folder = $opt_f ? $opt_f : "INBOX";

if(!$imap)
{
  printf("Can't talk to IMAP server $opt_h with $@.\n");
  printf("Can't check mail in account $opt_u\n");
  exit(1);
}

my $err_str;

# Leaving this here in case someone wants to extend it to check multiple folders
#my $folders = $imap->folders;
#if(!$folders)
#{
#  $err_str = $imap->LastError;
#  printf("Can't get folder list for $IMAP_User with $err_str.\n");
#  exit(1);
#}
#print "folders: @$folders\n";

if(!$imap->select($IMAP_folder) || $imap->LastError)
{
  $err_str = $imap->LastError;
  printf("Can't select folder \"$IMAP_folder\" for $opt_u with $err_str.\n");
  exit(1);
}

my $check_time = time() - $timeout;
my $delete_time = $check_time - $timeout;

my $status;
my @msgs;
@msgs = $imap->messages();
if($imap->LastError)
{
  $err_str = $imap->LastError;
  printf("Can't get message list with $err_str.\n");
  exit(1);
}

if($#msgs == -1)
{
  printf("No messages in account\n");
  exit(1);
}

my $delcount = 0;
my $newcount = 0;

foreach my $msg (@msgs)
{
  my $delivery_date = $imap->internaldate($msg);
  my $delivery_secs = str2time($delivery_date);
#  printf("msg:$msg, date:$delivery_date, delivery_secs:$delivery_secs\n");
  if($delivery_secs < $delete_time)
  {
    if(!$imap->delete_message($msg))
    {
      $err_str = $imap->LastError;
      printf("Can't delete old message with $err_str.\n");
      exit(1);
    }
    $delcount++;
  }
  if($delivery_secs > $check_time)
  {
    $newcount++;
  }

  my @received = $imap->parse_headers( $msg, "Received" );;
  my @rec_date;
  my @rec_host;
  push(@rec_date, $delivery_secs);
  push(@rec_host, "Delivery");
  foreach (@received)
  {
    my $rdate = $_;
    my $host = $_;
    $rdate =~ s/^.*; //;
    $host =~ s/^.*by //;
    $host =~ s/ \(.*$//;
    push(@rec_date, str2time($rdate));
    push(@rec_host, $host);
  }
  my $header_date = $imap->get_header($msg, "Date" );
  push(@rec_date, str2time($header_date));
  push(@rec_host, "Sender");

  my $subject = $imap->get_header($msg, "Subject");
  my $msgid = $imap->get_header($msg, "Message-ID");
  if(not $msgid)
  {
    print "message without Message-Id header (subject $subject)\n";
    exit(1);
  }

  for(my $i = 0; $rec_date[$i+1]; $i++)
  {
    my $skew = $rec_date[$i+1] - $rec_date[$i];
    my $delay = -$skew;
    if($skew > $max_skew)
    {
      print "Clock skewed\n";
      print "$rec_host[$i] received message $skew seconds before $rec_host[$i+1] sent it\nSubject: $subject\nMessage-ID: $msgid\n";
      if($delcount)
      {
        $imap->expunge();
        $err_str = $imap->LastError;
        if($imap->LastError)
        {
          printf("Can't expunge old message with $err_str.\n");
        }
      }
      exit(1);
    }
    elsif($delay > $max_delay)
    {
      print "Message delayed $delay seconds\n";
      print "$rec_host[$i] received message $delay seconds after $rec_host[$i+1] sent it\nSubject: $subject\nMessage-ID: $msgid\n";
      if($delcount)
      {
        $imap->expunge();
        $err_str = $imap->LastError;
        if($imap->LastError)
        {
          printf("Can't expunge old message with $err_str.\n");
        }
      }
      exit(1);
    }
  }
}
if($delcount)
{
  $imap->expunge();
  $err_str = $imap->LastError;
  if($imap->LastError)
  {
    printf("Can't expunge old message with $err_str.\n");
    exit(1);
  }
}
$imap->disconnect;

if($newcount)
{
  printf("Del $delcount msgs, found $newcount msgs\n");
  exit(0);
}
printf("Deleted $delcount messages and no new messages\n");
exit(1);
