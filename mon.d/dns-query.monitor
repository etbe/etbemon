#!/usr/bin/perl
#
# very straightforward dns monitor for use with "mon"
#
# arguments:
#       -t timeout         timeout (defaults to 5 seconds)
#       -n name            name to query, defaults to "mailhost"
#
# $Id: dns-query.monitor,v 1.1.1.1 2004/06/09 05:18:06 trockij Exp $
#
#    Copyright (C) 2003, Jim Trocki
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
use strict;

use Getopt::Std;
use Net::DNS::Resolver;

my %opt;
getopts ("t:n:", \%opt);

my $TIMEOUT = defined $opt{"t"} ? $opt{"t"} : 5;
my $NAME = $opt{"n"} || "mailhost";

my $r = Net::DNS::Resolver->new;

if (!defined $r)
{
    die "could not create new Net::DNS::Resolver object\n";
}

$r->udp_timeout ($TIMEOUT);

my (%good, %bad);

foreach my $server (@ARGV)
{
    $r->nameservers ($server);

    my $p = $r->search ($NAME);

    if (!defined $p)
    {
	$bad{$server}->{"detail"} = $r->errorstring;
    }

    else
    {
	my $n = $p->{"answer"}->[0];

	$good{$server}->{"detail"} = "$n->{name} $n->{class} $n->{type} $n->{address}";
    }
}

if (keys %bad)
{
    print join (" ", sort keys %bad), "\n";
}

else
{
    print "\n";
}

if (keys %bad)
{
    print "failures:\n";
    foreach my $k (keys %bad)
    {
    	print "    $k: $bad{$k}->{detail} ($NAME)\n";
    }

    print "\n";
}

if (keys %good)
{
    print "successes:\n";

    foreach my $k (keys %good)
    {
    	print "    $k: successfull lookup for $good{$k}->{detail} ($NAME)\n";
    }
}

exit 1 if (keys %bad);
exit 0;
