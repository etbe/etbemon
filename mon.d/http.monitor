#!/usr/bin/perl
use strict;
#
# Use try to connect to a http server.
# For use with "mon".
#
# http.monitor [-p port] [-t secs] [-u url] [-a agent] [-o] host [host...]
#
#    -p port       TCP port to connect to (defaults to 80)
#    -t secs       timeout, defaults to 30
#    -u url        path to get, defaults to "/"
#    -a agent      User-Agent, default to "mon.d/http.monitor"
#    -o            omit http headers from healthy hosts
#    -m regex      match regex in response (header + content)
#
# Jon Meek
# American Cyanamid Company
# Princeton, NJ
#
# $Id: http.monitor,v 1.1.1.1.4.1 2007/05/08 11:05:48 trockij Exp $
#
#    Copyright (C) 1998, Jim Trocki
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
use Getopt::Std;
use English;
use Data::Dumper;

sub httpGET;

our $opt_p;
our $opt_t;
our $opt_u;
our $opt_a;
our $opt_m;
our $opt_o;

getopts ("p:t:u:a:m:o");
my $PORT = $opt_p || 80;
my $TIMEOUT = $opt_t || 30;
my $URL = $opt_u || "/";
my $USERAGENT = $opt_a || "mon.d/http.monitor";
my $MATCHRE = $opt_m;

my %good;
my %bad;

exit 0 if (!@ARGV);

foreach my $host (@ARGV) {
    my $result = httpGET ($host, $PORT);

    if (!$result->{"ok"}) {
    	$bad{$host} = $result;
    } else {
    	$good{$host} = $result;
    }
}

my $ret;

if (keys %bad) {
    $ret = 1;
    print join (" ", sort keys %bad), "\n";
} else {
    $ret = 0;
    print "\n";
}

foreach my $h (keys %bad) {
    print "HOST $h: $bad{$h}->{error}\n";
    if ($bad{$h}->{"header"} ne "") {
	print $bad{$h}->{"header"}, "\n";
    }
    print "\n";
}

if (!$opt_o)
{
    foreach my $h (keys %good) {
	print "HOST $h: ok\n";
	print $good{$h}->{"header"}, "\n";
	print "\n";
    }
}

exit $ret;


sub httpGET {
    use Socket;
    use Sys::Hostname;

    my($Server, $Port) = @_;
    my($ServerOK, $TheContent);

    $TheContent = '';

    my $Path = $URL;

    my $result = {
    	"ok" => 0,
	"error" => undef,
	"header" => undef,
    };

###############################################################
    eval {
	local $SIG{ALRM} = sub { die "Timeout Alarm" };
	alarm $TIMEOUT;

	my $err = &OpenSocket($Server, $Port); # Open a connection to the server

	if ($err ne "") { # Failure to open the socket
	    $result = {
	    	"ok" => 0,
		"error" => $err,
		"header" => undef,
	    };

	    return undef;
	}

	print S "GET $Path HTTP/1.0\r\n";
	print S "Host: $Server\r\n";
	print S "User-Agent: $USERAGENT\r\n\r\n";

	while (my $in = <S>) {
	    $TheContent .= $in;  # Store data for later processing
	}

	close(S);
	alarm 0; # Cancel the alarm
    };

    ($result->{"header"}) = ($TheContent =~ /^(.*?)\r?\n\r?\n/s);

    if ($TheContent =~ /^HTTP\/([\d\.]+)\s+(200|30[12]|401)\b/) {
	$result->{"ok"} = 1;
    } else {
	$result->{"ok"} = 0;
	$result->{"error"} = "HTTP response code failure";
    }

    if ($MATCHRE ne "") {
	if ($TheContent =~ /$MATCHRE/s) {
	    $result->{"ok"} = 1;
	} else {
	    $result->{"ok"} = 0;
	    $result->{"error"} = "Regex match failed";
	}
    }

# check $result->{"ok"} as timeout isn't a problem if we already got good data
# should rewrite this to use a http library
    if ($result->{"ok"} != 1 && $EVAL_ERROR and ($EVAL_ERROR =~ /^Timeout Alarm/)) {
	$result->{"ok"} = 0;
	$result->{"error"} = "timeout after $TIMEOUT seconds";
    }

    return $result;
}

#
# Make a Berkeley socket connection between this program and a TCP port
# on another (or this) host. Port can be a number or a named service
#
# returns "" on success, or an error string on failure
#
sub OpenSocket {
    my ($host, $port) = @_;

    my $proto = (getprotobyname('tcp'))[2];

    return ("could not get protocol") if (!defined $proto);

    my $conn_port;

    if ($port =~ /^\d+$/) {
    	$conn_port = $port;

    } else {
	$conn_port = (getservbyname($port, 'tcp'))[2];
	return ("could not getservbyname for $port")
		if (!defined $conn_port);
    }

    my $host_addr = (gethostbyname($host))[4];

    return ("gethostbyname failure")
    		if (!defined $host_addr);

    my $that = sockaddr_in ($conn_port, $host_addr);

    if (!socket (S, &PF_INET, &SOCK_STREAM, $proto)) {
    	return ("socket: $!");
    }

    if (!connect (S, $that)) {
    	return ("connect: $!");
    }

    select(S); $| = 1; select(STDOUT);

    "";
}
