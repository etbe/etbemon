README.Debian:
--------------

* New and deprecated alerts and monitors in mon-1.2.0:
------------------------------------------------------
 - Added alerts:
   � irc.alert

 - Added monitors:
   � dns-query.monitor
   � local-syslog.monitor
   � lpd.monitor
   � ntpdate.monitor
   � radius.monitor
   � snmpdiskspace.monitor
   � snmpvar.monitor
   � trace.monitor

 - Removed alerts:
   � remote.alert

 - Removed monitors:
   � http_t.monitor
   � http_tp.monitor
   � http_tpp.monitor
    
 - Rename monitors:
   � dialin.monitor to dialin.monitor.wrap



* Daemon runs under 'mon' user:
-------------------------------
The daemon is not longer running as 'daemon' user.

Since version 0.99.2-12, mon runs under user 'mon', that is created
at the package installation/update process.



* Daemon control via /etc/default/mon:
--------------------------------------
Now 'mon' daemon can be controlled through /etc/default/mon directly.
  
These 2 options allows you to control the daemon behaviour:

- ENABLED:
  If you don't want to run 'mon' at boot time, please ensure the variable 
  ENABLED in /etc/default/mon is set to something different to : "yes", "true" or "1"

  Default is: ENABLED="yes"

- DAEMON_OPTS:
  Additional options can be passed to the daemon by adding them to DAEMON_OPTS. 

  Default is DAEMON_OPTS=" -f -c /etc/mon/mon.cf -A /etc/mon/auth.cf"



* Using CGI:
------------
If a webserver with CGI support is available on your system, both web interfaces 
can be accessed from the following URL:

  http://<yourhost>/cgi-bin/monshow.cgi

OR

  http://<yourhost>/cgi-bin/mon.cgi


The layout and behaviour of both interfaces, can be modified via an independent
configuration files.

Changes on 'monshow.cgi' should be made via it's configuration 
file '/etc/mon/monshowrc'


For details see:

  /usr/share/doc/mon/README.cgi-bin
  /usr/share/doc/mon/examples/example.monshowrc



* Using local monitors:
-----------------------
Under Debian GNU/Linux, paths for alerts and monitors differs from the original 
paths provided by software in order to comply with FHS.

Our alerts are located under '/usr/lib/mon/alert.d' and our monitors under 
'/usr/lib/mon/mon.d' directory.

Please take this into account if your intend to add your own scripts.

